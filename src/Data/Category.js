const Category = [
    {
        key: 1, 
        has_sub_categories: false, 
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2FRamadan-Images10-Oriental.webp?alt=media&token=1fe5bfcd-a16d-4e19-8980-22c43e9144f5",
        titleEn: "oriental_juices", 
        titleAr: "عصائر شرقية", 
    },{
        key: 2, 
        has_sub_categories: false, 
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2FiStock-1253099922-e1624432508767.jpg?alt=media&token=fee13810-cbd0-4c97-870e-e93aa341e0bb",
        titleEn: "nutural_juices", 
        titleAr: "عصائر طبيعية", 
    },{
        key: 3, 
        has_sub_categories: false, 
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2Fistockphoto-904617420-612x612.jpg?alt=media&token=40272091-79b0-4359-a1fe-e6a50480ea11",
        titleEn: "mango_world", 
        titleAr: "عالم المانجو", 
    },{
        key: 5, 
        has_sub_categories: false, 
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2FGettyImages-88179070-5826aff03df78c6f6a67db50.jpg?alt=media&token=da8033de-08d8-4eb2-a408-f5f61b4bfd63",
        titleEn: "choco_world", 
        titleAr: "عشاق الشيكولاته", 
    },{
        key: 6, 
        has_sub_categories: false, 
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2Fistockphoto-685816670-612x612.jpg?alt=media&token=a4f63beb-0f5c-4b6e-b93a-07bf7b12de71",
        titleEn: "ice_cream", 
        titleAr: "ايس كريم", 
    },{
        key: 7, 
        has_sub_categories: false, 
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2F201905170127562756.jpg?alt=media&token=85ef1a0d-4370-44d9-a966-ab920622d291",
        titleEn: "jungle_feedings", 
        titleAr: "Jungle Feedings", 
    },{
        key: 8, 
        has_sub_categories: false, 
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2F5-Frozen-Fruit-Smoothies-copy.jpg?alt=media&token=ae1a0420-a65d-4a5b-9f01-3f4cde80afb1",
        titleEn: "farghly_smoothie", 
        titleAr: "سموزى فرغلى", 
    },{
        key: 9, 
        has_sub_categories: false, 
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2Flandscape-1433889344-del-milkshakes-index.jpg?alt=media&token=d2cd715a-588c-4acf-a95d-33b6a7d909f4",
        titleEn: "milkshake", 
        titleAr: "ميلك شيك", 
    },{
        key: 10, 
        has_sub_categories: false, 
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2F7CVGBe3fs78gePtkYs9ALVIr0Gi3HFv8pew8n7Rr.jpg?alt=media&token=1c57a3ba-6396-4cb9-989c-e6ffb56cae84",
        titleEn: "zabado", 
        titleAr: "زبادووو", 
    },{
        key: 11, 
        has_sub_categories: false, 
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2Fset-pecan-pistachios-almond-peanut-cashew-pine-nuts-lined-up-assorted-nuts-dried-fruits-mini-different-bowls_4b097fbb-6d4b-4261-a76b-eb6b579cae9e.webp?alt=media&token=cb8ae857-c13c-4ea8-9791-b1bccc0bc7e8",
        titleEn: "farghaly_nuts", 
        titleAr: "محمصات فرغلى", 
    },{
        key: 12, 
        has_sub_categories: false, 
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2FHub_JuiceCleanse_Social_1200x628.webp?alt=media&token=b75d2e47-d90d-42d8-8e95-8b3bd16b37cd",
        titleEn: "farghaly_diet", 
        titleAr: "دايت فرغلى", 
    },{
        key: 13, 
        has_sub_categories: false, 
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2Ffresh-juice-mix-fruit-healthy-drinks-wooden-table-42545817.jpg?alt=media&token=3e9a3294-1771-4bfd-a600-e77e1092dc85",
        titleEn: "farghaly_mix", 
        titleAr: "مكسات فرغلى", 
    },
    // {
    //     key: 14, 
    //     has_sub_categories: false, 
    //     image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2Fistockphoto-539246687-612x612.jpg?alt=media&token=8d4faf22-6e4d-43ef-8387-10153b08aff5",
    //     titleEn: "nutella_waffel", 
    //     titleAr: "قسم النوتيلا + الوافل", 
    // },
    {
        key: 15, 
        has_sub_categories: false, 
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2Fdownload.jpeg?alt=media&token=49cf4d9e-04bc-4c51-b558-232123023e2d",
        titleEn: "height_power", 
        titleAr: "الباور العالى", 
    }, {
        key: 17, 
        has_sub_categories: false, 
        image: "https://www.chelseasmessyapron.com/wp-content/uploads/2014/04/FRUIT-SALAD-3-500x500.jpg",
        titleEn: "salad_fruit", 
        titleAr: "Salad Fruits", 
    },{ 
        key: 18, 
        has_sub_categories: false, 
        image: "https://img.taste.com.au/dKlHzXHw/taste/2016/11/milk-rice-pudding-25127-1.jpeg",
        titleEn: "rice_milk", 
        titleAr: "ارز باللبن", 
    },{
        key: 16, 
        has_sub_categories: false, 
        image: "https://firebasestorage.googleapis.com/v0/b/eagles-resort.appspot.com/o/farghly_drinks%2Fdownload%20(1).jpeg?alt=media&token=2ca990f0-e00a-4656-8963-c84c56673eeb",
        titleEn: "other_additions", 
        titleAr: "اضافات اخرى", 
    },
   
]   


export default Category